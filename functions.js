function validar() {
    var maisculas = "ABCDEFGHYJKLMNOPQRSTUVWXYZ";
    var descricao = form1.descricao.value;
    var valido = true;

    if (descricao == "") {
      alert('Preencha o campo com a descricao');
      form1.descricao.focus();
      return false;
    }

    if (descricao.length < 6) {
      alert('O campo descricao deve ter mais que 6 caracteres');
      form1.descricao.focus();
      return false;
    }

    for(i=0; i<maisculas.length; i++){
      if (descricao.charAt(0) == maiusculas.charAt(i)){
         valido = true;
      }
      else{
        valido = false;
      }
    }
     return valido;
}
