<?php
	require_once( "./comum.php");
	require_once( BASE_DIR . "/classes/Categoria.php");

	session_start();
	if( !isset( $_SESSION["usuario"] ) )
	{
		Header("location: inicio.php");
	}


?>
<html>
<head>
	<meta charset="UTF-8" />
	<title>Bazar Tem Tudo</title>
	<script language="Javascript">
			function excluir(categoria, url) {
				if( confirm('Confirma a exclusão do registro '+categoria+'?') )
						location.href = url;
			}

			function atualizar(categoria, url) {
				if( confirm('Deseja alterar o registro '+categoria+'?') )
						location.href = url;
			}
	</script>
</head>
<body>

	<?php require_once("cabecalho.inc"); ?>

	<div id="corpo">
		<?php require_once("incluirCategoriaForm.php") ?>
		<tr>
		</tr>
		<table border="1">
			<thead>
				<tr>
					<th>Código</th>
					<th>Descrição</th>
					<th>Taxa</th>
				</tr>
			</thead>
			<tbody>
				<?php
				$categorias = Categoria::findAll();
				foreach( $categorias as $categoria) {
				?>
				<tr>
					<td><?= $categoria->getIdCategoria(); ?></td>
					<td><?= $categoria->getDescricao(); ?></td>
					<td><?= $categoria->getTaxa(); ?></td>
					<td>
						<input type="button" onclick="excluir( '<?php echo $categoria->getDescricao(); ?>', '<?php echo 'excluirCategoria.php?id='.$categoria->getIdCategoria().'&descricao='.$categoria->getDescricao()?>')" value="Excluir">
					</td>
					<td>
						<input type="button" onclick="atualizar( '<?php echo $categoria->getDescricao(); ?>', '<?php echo 'alterarCategoriaForm.php?id='.$categoria->getIdCategoria().'&descricao='.$categoria->getDescricao()?>')" value="Alterar">
					</td>
				</tr>
				<?php
				}
				?>
			</tbody>
		</table>
	</div>

	<?php require_once("rodape.inc"); ?>

</body>
</html>
